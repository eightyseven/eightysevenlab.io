import { Component, OnInit, AfterContentChecked } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.sass']
})
export class PersonComponent implements OnInit, AfterContentChecked {

id: string;

constructor(private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
  }

  ngAfterContentChecked() {
      console.log('hello');
      this.activatedRoute.params.subscribe(params => {
            console.log({params});
            this.id = params["id"];
        });
  }

}
