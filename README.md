# Eightyseven

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.4.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

chunk {0} runtime-es2015.a93f27da30cdae68103d.js (runtime) 2.82 kB [entry] [rendered]
chunk {1} main-es2015.a0bd83b3f835163b2f3b.js (main) 490 kB [initial] [rendered]
chunk {2} polyfills-es2015.d56dcd69a3ec19d2c4cd.js (polyfills) 64.1 kB [initial] [rendered]
chunk {3} polyfills-es5-es2015.9a1d06805593c9dc79e8.js (polyfills-es5) 222 kB [initial] [rendered]
chunk {4} styles.eb8e96d5adaf3e605478.css (styles) 61 kB [initial] [rendered]
Date: 2019-09-13T02:20:45.012Z - Hash: 23e07ba66922264233e8 - Time: 26647ms
Generating ES5 bundles for differential loading...
ES5 bundle generation complete.
